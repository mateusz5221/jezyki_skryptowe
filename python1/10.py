#!/usr/bin/python


delete = {" i": " oraz",
          " oraz": " i" ,
          " nigdy": " prawie nigdy",
          " dlaczego": " czemu",
          " I": " Oraz",
          " Oraz": " I",
          " Nigdy": " Prawie nigdy",
          " Dlaczego": " Czemu"}

file_input = open("pan-tadeusz-czyli-ostatni-zajazd-na-litwie.txt")
file_output = open("output10.txt", "w+")
for line in file_input:
    for word in delete:
        line = line.replace(word, delete[word])
    file_output.write(line)
file_input.close()
file_output.close()

