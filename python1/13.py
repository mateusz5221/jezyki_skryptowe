#!/usr/bin/python

import numpy as np

def matrix_mul(A, B):
    n = np.shape(A)[0]
    m = np.shape(A)[1]
    if not m == np.shape(B)[0]:
        return None
    p = np.shape(B)[1]
    C = np.zeros((n, p))
    for i in range(n):
        for j in range(p):
            temp = 0
            for k in range(m):
                temp += A[i][k] * B[k][j]
            C[i][j] = temp
    return C

size = 8

A = np.random.randint(-10, 10, (size, size))
B = np.random.randint(-10, 10, (size, size))

print(matrix_mul(A, B))

