#!/usr/bin/python

import numpy as np

size = 128

A = np.random.randint(0, 100, (size, size))
B = np.random.randint(0, 100, (size, size))
C = np.zeros((size, size))

for i in range(size):
    for j in range(size):
        C[i][j] = A[i][j] + B[i][j]

print(C)

