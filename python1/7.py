#!/usr/bin/python

import math

a = int(input("Podaj a"))
b = int(input("Podaj b"))
c = int(input("Podaj c"))

d = math.pow(b, 2)-4*a*c
if a == 0:
    if b == 0:
        print("Brak miejsc zerowych")
    else:
        print(-c/b )
else:
    if d > 0:
        print((-b-math.sqrt(d))/(2*a), (-b+math.sqrt(d))/(2*a))
    elif d == 0:
        print(-b/(2*a))
    else:
        print("Brak miejsc zerowych")
