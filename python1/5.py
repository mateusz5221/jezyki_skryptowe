#!/usr/bin/python

import os

def get_into_dir(path, tab):
    tab += 1
    for file in os.listdir(path):
        if os.path.isdir(path + '/' + file):
            print(tab * '\t' + file + '/')
            get_into_dir(path + '/' + file, tab)
        else:
            print(tab * '\t' + file)

get_into_dir("/dev", 0)

