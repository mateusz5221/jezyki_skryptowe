#!/usr/bin/python

import os

for i in range(4):
    file = open(str(i) + ".jpg", "w")
    file.close()

for file in os.listdir():
    file_name = os.path.splitext(file)
    if file_name[1] == ".jpg":
        os.rename(file, file_name[0] + ".png")
