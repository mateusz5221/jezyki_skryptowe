#!/usr/bin/python

import numpy as np
import math

def det(A):
    n = np.shape(A)
    if n[0]== n[1]:
        n = n[0]
    else:
        return None
    if n == 1:
        return A
    elif n == 2:
        result = A[0][0]*A[1][1]-A[1][0]*A[0][1]
        return result
    else:
        result = 0
        for i in range(n):
            B = []
            for j in range(n):
                if i == j:
                    continue
                else:
                    B.append(A[j][1:])
            result += A[i][0] * math.pow(-1, i) * det(B)
        return result

size = 8
A = np.random.randint(-10, 10, (size, size))
print(det(A))
print(np.linalg.det(A))
