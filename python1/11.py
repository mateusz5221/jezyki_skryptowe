#!/usr/bin/python

a = [1, 2, 12, 4]
b = [2, 4, 2, 8]

result = 0
for i in range(min([len(a), len(b)])):
    result += a[i] * b[i]
print(result)
