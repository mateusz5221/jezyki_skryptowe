#!/usr/bin/python

import xml.sax

# Script with SAX

class CDHandler(xml.sax.ContentHandler):
    def __init__(self):
        self.CurrentData = ""
        self.title = ""
        self.artist = ""
        self.contry = ""
        self.company = ""
        self.price = ""
        self.year = ""

    def startElement(self, tag, attributes):
        self.CurrentData = tag
        if tag == "CD":
            print("--CD--")
            id = attributes["id"]
            print("ID: " + id)

    def endElement(self, tag):
        if self.CurrentData == "TITLE":
            print("Title: " + self.title)
        elif self.CurrentData == "ARTIST":
            print( "Artist: " + self.artist)
        elif self.CurrentData == "COUNTRY":
            print("Country: " + self.country)
        elif self.CurrentData == "COMPANY":
            print("Company: " + self.company)
        elif self.CurrentData == "PRICE":
            print("Price: " + self.price)
        elif self.CurrentData == "YEAR":
            print("Year: " + self.year)
        self.CurrentData = ""

    def characters(self, content):
        if self.CurrentData == "TITLE":
            self.title = content
        elif self.CurrentData == "ARTIST":
            self.artist = content
        elif self.CurrentData == "COUNTRY":
            self.country = content
        elif self.CurrentData == "COMPANY":
            self.company = content
        elif self.CurrentData == "PRICE":
            self.price = content
        elif self.CurrentData == "YEAR":
            self.year = content


parser = xml.sax.make_parser()
parser.setFeature(xml.sax.handler.feature_namespaces, 0)
Handler = CDHandler()
parser.setContentHandler(Handler)
parser.parse("cd_catalog.xml")
