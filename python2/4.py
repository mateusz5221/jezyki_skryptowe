#!/usr/bin/python

import threading, random

def hist(filename):
    # prepare array
    file = open(filename, "r")
    array = []
    for item in file:
        array.append(int(item))
    file.close()
    # compute histogram
    array_dict = dict.fromkeys(array, 0)
    for item in array:
        array_dict[item] += 1
    print(array_dict, sum(array_dict))


proc1 = threading.Thread(target=hist, args=("array1.txt", ))
proc2 = threading.Thread(target=hist, args=("array2.txt", ))

proc1.start()
proc2.start()

