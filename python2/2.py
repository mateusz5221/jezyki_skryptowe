#!/usr/bin/python

import math

class Complex:
    def __init__(self, r, i):
        self.r = r
        self.i = i

    def get(self):
        return (self.r, self.i)

    def abs(self):
        return math.sqrt(pow(self.r,2) + pow(self.i,2))

    def add(self, other):
        return (self.r + other.r, self.i + other.i)

    def sub(self, other):
        return (self.r - other.r, self.i - other.i)

    def mul(self, other):
        return Complex(self.r * other.r - self.i * other.i,
                       self.r * other.i + self.i * other.r).get()

    def div(self, other):
        r = float(pow(other.r, 2) + pow(other.i, 2))
        return Complex((self.r*other.r + self.i*other.i)/r,
                       (self.i*other.r - self.r*other.i)/r).get()

    def neg(self):
        return (-self.r, -self.i)

    def equ(self, other):
        if self.r == other.r and self.i == other.i:
            return True
        else:
            return False

    def con(self):
        return (self.r, -self.i)

    def pow(self):
        return self.mul(self)


x = Complex(1,1)
y = Complex(2,2)
z = Complex(1,1)
print(x.get())
print(x.abs())
print(x.add(y))
print(x.sub(y))
print(x.mul(y))
print(x.div(y))
print(x.neg())
print(x.equ(z))
print(x.con())
print(x.pow())
