#!/usr/bin/python

from xml.dom.minidom import parse
import xml.dom.minidom

# Script with DOM

DOMTree = xml.dom.minidom.parse("cd_catalog.xml")
collection = DOMTree.documentElement

cds = collection.getElementsByTagName("CD")
for cd in cds:
    print("--CD--")
    if cd.hasAttribute("id"):
        print("ID: " + cd.getAttribute("id"))
        author = cd.getElementsByTagName('TITLE')[0]
        print("Title: " + author.childNodes[0].data)
        title = cd.getElementsByTagName('ARTIST')[0]
        print("Artist: " + title.childNodes[0].data)
        genre = cd.getElementsByTagName('COUNTRY')[0]
        print("Country: " + genre.childNodes[0].data)
        price = cd.getElementsByTagName('COMPANY')[0]
        print("Company: " + price.childNodes[0].data)
        publish_date = cd.getElementsByTagName('PRICE')[0]
        print("Price: " + publish_date.childNodes[0].data)
        description = cd.getElementsByTagName('YEAR')[0]
        print("Year: " + description.childNodes[0].data)


def replaceText(collection, node, valueOfNode, newText):
    cds = collection.getElementsByTagName("CD")
    for cd in cds:
        if cd.hasAttribute("id"):

            if cd.getElementsByTagName(node)[0].childNodes[0].data == valueOfNode:

                cd.getElementsByTagName(node)[0].childNodes[0].replaceWholeText(newText)

